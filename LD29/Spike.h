#ifndef SPIKE_H
#define SPIKE_H

#include "MEngine/MEngine.h"

#include "Player.h"

class Spike
{
public:
	Spike();
	~Spike();

	void Update(Global *global);
	void Draw(Global *global);

	void SetPosition(Rect& pos);
	void SetPlayer(Player* player);
	void SetIsTop(bool top);
	void SetActive(bool active);
	void SetX(int x);

private:
	Rect pos;

	Player* player;

	bool active;

	bool top;
	int speed;
};

#endif