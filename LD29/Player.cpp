#include "Player.h"

Player::Player()
{
	pos.SetPosition(1024 / 2 - 24, 720 - 96, 48, 96);
	dead = false;
	points = 0;
	highScore = 0;
}

Player::~Player()
{
}

void Player::Update(Global *global)
{
	if (!dead)
	{
		if (pos.Bottom() < 720 && !global->input.keyboard.IsKeyPressed(SDLK_w))
			pos.TranslateY(+10);

		//Player Movement
		if (global->input.keyboard.IsKeyPressed(SDLK_d) && pos.Right() < global->screen.GetScreenSize()->getW())
			pos.TranslateX(+5);
		if (global->input.keyboard.IsKeyPressed(SDLK_a) && pos.getX() > 0)
			pos.TranslateX(-5);

		if (global->input.keyboard.IsKeyPressed(SDLK_w) && pos.getY() > 48)
			pos.TranslateY(-10);

		points++;
	}
}

void Player::Draw(Global *global)
{
	global->gfx.DrawText("FONT", CreateString("Points: ", NumberToString(points), "").c_str() , 24, &Rect::CreateRect(0, 0, 0, 0), MapRGB(0, 0, 0));
	
	if (global->input.keyboard.IsKeyPressed(SDLK_w) && !dead)
		global->gfx.DrawTexture("PLAYER", &pos);
	else
		global->gfx.DrawTexture("PLAYER2", &pos);
}

//Setters
void Player::SetPosition(Rect& pos)
{
	this->pos = pos;
}

void Player::SetDead(bool dead)
{
	this->dead = dead;
}

void Player::SetPoints(int points)
{
	this->points = points;
}

void Player::SetHighScore(int highScore)
{
	this->highScore = highScore;
}

//Getters
Rect& Player::GetPosition()
{
	return pos;
}

bool& Player::GetDead()
{
	return dead;
}

int& Player::GetPoints()
{
	return points;
}

int& Player::GetHighScore()
{
	return highScore;
}