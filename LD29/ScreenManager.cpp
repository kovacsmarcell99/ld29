#include "ScreenManager.h"

ScreenManager::ScreenManager()
{
	top.LoadContent(0);
	bottom.LoadContent(768 - 48);

	top.SetPlayer(&player);
	bottom.SetPlayer(&player);

	for (int i = 0; i < spikeNum; i++)
		spike[i].SetPlayer(&player);

	timer = 0;

	max = 6;
	maxTimer = 0;

	nextSpike = 0;

	if (FileExits("data.save"))
	{
		std::ifstream in("data.save");
		
		int highScore = 0;
		in >> highScore;

		if (highScore != 0)
			player.SetHighScore(Decrypt(highScore));

		in.close();
	}
}

ScreenManager::~ScreenManager()
{
	std::ofstream out("data.save");

	int highScore = Encrypt(player.GetHighScore());

	out << highScore;

	out.close();
}

void ScreenManager::Update(Global *global)
{
	top.Update(global);
	bottom.Update(global);

	if (!player.GetDead())
	{
		for (int i = 0; i < spikeNum; i++)
			spike[i].Update(global);

		if (timer > 12)
		{
			if (RandomNumber(0, max) == 0)
			{
				spike[nextSpike].SetActive(true);
				spike[nextSpike].SetIsTop(RandomBool());
				spike[nextSpike].SetX(1024 + 48);

				nextSpike++;
			}

			if (maxTimer > 6)
			{
				if (max > 2)
					max--;

				maxTimer = 0;
			}
			maxTimer++;

			timer = 0;
		}

		if (nextSpike > spikeNum - 1)
			nextSpike = 0;

		timer++;
	}
	else
	{
		if (player.GetPoints() > player.GetHighScore())
			player.SetHighScore(player.GetPoints());
		if (global->input.keyboard.IsKeyPressed(SDLK_r))
		{
			for (int i = 0; i < spikeNum; i++)
			{
				spike[i].SetActive(false);
				spike[i].SetX(1024 + 48);
			}

			player.SetDead(false);
			player.SetPosition(Rect::CreateRect(1024 / 2 - 24, 720 - 96, 48, 96));
			player.SetPoints(0);
		}
	}

	player.Update(global);
}

void ScreenManager::Draw(Global *global)
{
	top.Draw(global);
	bottom.Draw(global);
	
	for (int i = 0; i < spikeNum; i++)
		spike[i].Draw(global);

	player.Draw(global);

	if (player.GetDead())
	{
		global->gfx.DrawText("FONT", "Press 'R' to Respawn!", 24, &Rect::CreateRect(1024 / 2 - 150, 768 / 2 - 10, 0, 0), MapRGB(255, 255, 0));
		global->gfx.DrawText("FONT", CreateString("Highscore: ",NumberToString(player.GetHighScore()),"").c_str(), 24, &Rect::CreateRect(1024 / 2 - 150, 768 / 2 - 10 + 40, 0, 0), MapRGB(255, 255, 0));
	}
}