#include "Stone.h"

Stone::Stone()
{
	solid = true;
	speed = 4;
}

Stone::~Stone()
{

}

void Stone::Update(Global* global)
{
	if (!player->GetDead())
	{
		pos.TranslateX(-speed);

		if (pos.Right() <= 0)
			pos.setX(global->screen.GetScreenSize()->getW() + 32);
	}
}

void Stone::Draw(Global *global)
{
	global->gfx.DrawTexture("STONE", &pos);
}


void Stone::SetPosition(Rect& pos)
{
	this->pos = pos;
}

void Stone::SetSpeed(int speed)
{
	this->speed = speed;
}

void Stone::SetPlayer(Player* player)
{
	this->player = player;
}