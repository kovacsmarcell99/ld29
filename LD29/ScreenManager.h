#ifndef SCREENMANAGER_H
#define SCREENMANAGER_H

#include "StoneLane.h"
#include "Player.h"

#include "Spike.h"

#include "MEngine/MEngine.h"

inline int Encrypt(int num)
{
	return -(num * 3);
}

inline int Decrypt(int num)
{
	return -(num / 3);
}

class ScreenManager
{
public:
	ScreenManager();
	~ScreenManager();

	void Update(Global *global);
	void Draw(Global *global);

private:
	StoneLane top;
	StoneLane bottom;
	
	Player player;

	int timer;

	int maxTimer;
	int max;

	static const short int spikeNum = 25;
	int nextSpike;
	Spike spike[spikeNum];
};

#endif