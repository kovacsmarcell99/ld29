#include "Spike.h"

Spike::Spike()
{
	pos.SetPosition(1024, 48, 48, 192);
	SetIsTop(false);
	speed = 4;
	active = false;
}

Spike::~Spike()
{
}

void Spike::Update(Global *global)
{
	if (!player->GetDead() && active)
	{
		if (pos.getX() > -pos.getW())
			pos.TranslateX(-speed);

		if (player->GetPosition().Intersects(pos))
				player->SetDead(true);
	}
}

void Spike::Draw(Global *global)
{
	if (top)
		global->gfx.DrawTextureFlip("SPIKE", &pos, SDL_FLIP_VERTICAL);
	else
		global->gfx.DrawTexture("SPIKE", &pos);
}

void Spike::SetPosition(Rect& pos)
{
	this->pos = pos;
}

void Spike::SetPlayer(Player* player)
{
	this->player = player;
}

void Spike::SetIsTop(bool top)
{
	this->top = top;
	
	if (top)
		pos.setY(48);
	else
		pos.setY(768 - pos.getH() - 48);
}

void Spike::SetActive(bool active)
{
	this->active = active;
}

void Spike::SetX(int x)
{
	pos.setX(x);
}