#ifndef STONE_H
#define STONE_H

#include "MEngine/MEngine.h"

#include "Player.h"
#include "Tile.h"


class Stone : public Tile
{
public:
	Stone();
	~Stone();

	void Update(Global *global);
	void Draw(Global *global);

	void SetSpeed(int speed);
	void SetPosition(Rect& pos);
	void SetPlayer(Player* player);

private:
	Player* player;

	int speed;
};

#endif // STONE_H