#ifndef PLAYER_H
#define PLAYER_H

#include "MEngine/MEngine.h"

class Player
{
public:
	Player();
	~Player();

	void Update(Global* global);
	void Draw(Global* global);

	void SetPosition(Rect& pos);
	void SetDead(bool dead);
	void SetPoints(int points);
	void SetHighScore(int highScore);

	Rect& GetPosition();
	bool& GetDead();
	int& GetPoints();
	int& GetHighScore();

private:
	Rect pos;

	int points;
	int highScore;

	bool dead;
};

#endif