#ifndef STONELANE_H
#define STONELANE_H

#include "Stone.h"

class StoneLane
{
public:
	StoneLane();
	~StoneLane();

	void LoadContent(int y);

	void Update(Global *global);
	void Draw(Global *global);

	void SetPlayer(Player* player);

private:
	static const short int stoneNum = 23;
	Stone stones[stoneNum];
};

#endif