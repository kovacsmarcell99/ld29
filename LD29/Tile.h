#ifndef TILE_H
#define TILE_H

#include "MEngine/MEngine.h"

class Tile
{
public:
	Tile();
	~Tile();

protected:
	bool solid;

	Rect pos;
};

#endif //TILE_H