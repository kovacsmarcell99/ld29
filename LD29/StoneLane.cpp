#include "StoneLane.h"

StoneLane::StoneLane()
{
}

StoneLane::~StoneLane()
{
}

void StoneLane::LoadContent(int y)
{
	for (int i = 0; i < stoneNum; i++)
	{
		stones[i].SetPosition(Rect::CreateRect(i * 48, y, 48, 48));
	}
}

void StoneLane::Update(Global *global)
{
	for (int i = 0; i < stoneNum; i++)
	{
		stones[i].Update(global);
	}
}

void StoneLane::Draw(Global *global)
{
	for (int i = 0; i < stoneNum; i++)
	{
		stones[i].Draw(global);
	}
}

void StoneLane::SetPlayer(Player* player)
{
	for (int i = 0; i < stoneNum; i++)
	{
		stones[i].SetPlayer(player);
	}
}