#include "MEngine/MEngine.h"

#include "ScreenManager.h"

int main(int argc, char *argv[])
{
	if (Init() != 0)
		return 1;

	Logger *logger = new Logger;

	//Create Window
	SDL_Window *window = CreateAndLogWindow("Don't fly in the Mine! v0.1 Pre-Alpha", 1024, 768, SDL_WINDOW_SHOWN, logger);

	if (window == NULL)
		return 1;

	//Create Renederer
	SDL_Renderer *renderer = NULL;
	renderer = CreateAndLogRenderer(window, logger, true, false);

	if (renderer == NULL)
		return 1;

	Global *global = new Global(window, renderer, logger);

	global->gfx.AddFont("resources/fonts/VCR_OSD_MONO.ttf", "FONT", 24);

	global->gfx.AddTexture("resources/textures/light.png", "LIGHT");
	global->gfx.AddTexture("resources/textures/stone.png", "STONE");
	global->gfx.AddTexture("resources/textures/spike.png", "SPIKE");

	global->gfx.AddTexture("resources/textures/player.png", "PLAYER");
	global->gfx.AddTexture("resources/textures/player2.png", "PLAYER2");

	global->audio.AddMusic("resources/audio/music/songnew.wav", "MUSIC");

	Timer *timer = new Timer();
	timer->Start();
	
	FPSTimer FPS(60.0f);
	FPS.Start();

	bool running = true;

	SDL_RenderSetLogicalSize(renderer, 1024, 768);

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	
	ScreenManager *screen = new ScreenManager();

	global->audio.PlayMusic("MUSIC");

	while (running)
	{
		if (FPS.Tick())
		{
			//Input
			while (SDL_PollEvent(&global->input.event))
			{
				if (global->input.event.type == SDL_QUIT)
					running = false;
				if (global->input.keyboard.IsKeyPressed(SDLK_ESCAPE))
					running = false;

				global->input.mouse.Update();
				global->input.keyboard.Update();
			}

			//Screenshot Creation
			if (global->input.keyboard.IsKeyPressedOnce(SDLK_F2))
				CreateScreenshot(window, renderer);

			//Update

			screen->Update(global);

			//Draw

			SDL_RenderClear(renderer);

			global->gfx.DrawTexture("LIGHT", &Rect::CreateRect(300, 300, 128, 128));
			screen->Draw(global);

			SDL_RenderPresent(renderer);

			//FPS StufF

			FPSCounter(timer);
		}
	}

	delete screen;
	delete global;
	delete timer;
	delete logger;

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

	Quit();

	return 0;
}